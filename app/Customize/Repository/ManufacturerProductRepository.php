<?php

namespace Customize\Repository;

use Customize\Entity\ManufacturerProduct;
use Eccube\Repository\AbstractRepository;
use Doctrine\Persistence\ManagerRegistry as RegistryInterface;

class ManufacturerProductRepository extends AbstractRepository
{
    /**
     * ManufacturerProductRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ManufacturerProduct::class);
    }
}
