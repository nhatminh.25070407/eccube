<?php

namespace Customize\Repository;

use Customize\Entity\Manufacturer;
use Eccube\Repository\AbstractRepository;
use Doctrine\Persistence\ManagerRegistry as RegistryInterface;
use Eccube\Doctrine\Query\Queries;
use Eccube\Util\StringUtil;

class ManufacturerRepository extends AbstractRepository
{
    /**
     * ManufacturerRepository constructor.
     *
     * @param RegistryInterface $registry
     */

    protected $queries;

    public const COLUMNS = [
        'manufacturer_id' => 'p.id',
        'name' => 'p.name',
        'create_date' => 'p.create_date',
        'update_date' => 'p.update_date'
    ];
    /**
     * ManufacturerRepository constructor.
     *
     * @param RegistryInterface $registry
     * @param Queries $queries
     *
     */
    public function __construct(RegistryInterface $registry, Queries $queries)
    {

        parent::__construct($registry, Manufacturer::class);
        $this->queries = $queries;
    }
    public function findAll () {
        $qb = $this->createQueryBuilder('p');
        $Manufacturers = $qb->getQuery()->getResult();
        // dd($Manufacturers);
        return $Manufacturers;
    }
    public function getQueryBuilderBySearchDataForAdmin($searchData)
    {
        $qb = $this->createQueryBuilder('p');
        // dump($searchData);
        // die;

            // id
        if (isset($searchData['id']) && StringUtil::isNotBlank($searchData['id'])) {
            $id = preg_match('/^\d{0,10}$/', $searchData['id']) ? $searchData['id'] : null;
            if ($id && $id > '2147483647' && $this->isPostgreSQL()) {
                $id = null;
            }
            $qb
                ->andWhere('p.id = :id OR p.name LIKE :likeid')
                ->setParameter('id', $id)
                ->setParameter('likeid', '%'.str_replace(['%', '_'], ['\\%', '\\_'], $searchData['id']).'%');
        }

        if (isset($searchData['sortkey']) && !empty($searchData['sortkey'])) {
            $sortOrder = (isset($searchData['sorttype']) && $searchData['sorttype'] == 'a') ? 'ASC' : 'DESC';

            $qb->orderBy(self::COLUMNS[$searchData['sortkey']], $sortOrder);
            $qb->addOrderBy('p.update_date', 'DESC');
            $qb->addOrderBy('p.id', 'DESC');
        } else {
            $qb->orderBy('p.update_date', 'DESC');
            $qb->addOrderBy('p.id', 'DESC');
        }

        return $this->queries->customize(QueryKey::MANUFACTURER_SEARCH_ADMIN, $qb, $searchData);

    }
}
