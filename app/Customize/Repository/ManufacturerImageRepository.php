<?php

namespace Customize\Repository;

use Customize\Entity\ManufacturerImage;
use Eccube\Repository\AbstractRepository;
use Doctrine\Persistence\ManagerRegistry as RegistryInterface;

class ManufacturerImageRepository extends AbstractRepository
{
    /**
     * ManufacturerImageRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ManufacturerImage::class);
    }
}
