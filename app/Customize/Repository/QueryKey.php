<?php

namespace Customize\Repository;

class QueryKey
{
    public const MANUFACTURER_SEARCH_ADMIN = 'Manufacturer.getQueryBuilderBySearchDataForAdmin';
}
