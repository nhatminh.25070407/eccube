<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Customize\Entity;

use Doctrine\ORM\Mapping as ORM;
use Eccube\Annotation\EntityExtension;

/**
 * @EntityExtension("Eccube\Entity\Product")
 */
trait ProductTrait
{
    /**
     * @var \Customize\Entity\Manufacturer
     *
     * @ORM\ManyToOne(targetEntity="Customize\Entity\Manufacturer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="manufacturer_id", referencedColumnName="id")
     * })
     */
    private $Manufacturer;

    /**
     * @return Manufacturer
     */
     public function getManufacturer()
    {
        return $this->Manufacturer;
    }
     public function setManufacturer(Manufacturer $Manufacturer = null)
    {
        return $this->Manufacturer = $Manufacturer;
    }
}
