<?php

namespace Customize\Controller\Admin;

use Customize\Entity\Manufacturer;
use Customize\Entity\ManufacturerImage;
use Customize\Form\Type\Admin\ManufacturerType;
use Customize\Repository\ManufacturerImageRepository;
use Customize\Repository\ManufacturerRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Eccube\Common\Constant;
use Eccube\Controller\AbstractController;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Eccube\Form\Type\Admin\SearchProductType;
use Eccube\Repository\BaseInfoRepository;
use Eccube\Repository\Master\PageMaxRepository;
use Eccube\Repository\Master\ProductListMaxRepository;
use Eccube\Util\CacheUtil;
use Eccube\Util\FormUtil;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Symfony\Component\Routing\RouterInterface;

class ManufacturerController extends AbstractController
{
    /**
     * @var BaseInfo
     */
    protected $BaseInfo;

    /**
     * @var ProductListMaxRepository
     */
    protected $productListMaxRepository;

    protected ManufacturerRepository $manufacturerRepository;
    protected PageMaxRepository $pageMaxRepository;
    protected ManufacturerImageRepository $manufacturerImageRepository;

    public function __construct(
        BaseInfoRepository $baseInfoRepository,
        ManufacturerRepository $manufacturerRepository,
        PageMaxRepository $pageMaxRepository,
        ProductListMaxRepository $productListMaxRepository,
        ManufacturerImageRepository $manufacturerImageRepository
    ) {
        $this->manufacturerRepository = $manufacturerRepository;
        $this->BaseInfo = $baseInfoRepository->get();
        $this->productListMaxRepository = $productListMaxRepository;
        $this->pageMaxRepository = $pageMaxRepository;
        $this->manufacturerImageRepository = $manufacturerImageRepository;
    }

    /**
     * 商品一覧画面.
     *
     * @Route("/%eccube_admin_route%/manufacturer", name="admin_manufacturer", methods={"GET", "POST"})
     * @Route("/%eccube_admin_route%/manufacturer/page/{page_no}", requirements={"page_no" = "\d+"}, name="admin_manufacturer_page", methods={"GET", "POST"})
     * @Template("@admin/Manufacturer/index.twig")
     */
    public function index(Request $request, PaginatorInterface $paginator, $page_no = null)
    {
        $builder = $this->formFactory
            ->createBuilder(SearchProductType::class);

        $event = new EventArgs(
            [
                'builder' => $builder,
            ],
            $request
        );
        $this->eventDispatcher->dispatch($event, EccubeEvents::ADMIN_PRODUCT_INDEX_INITIALIZE);

        $searchForm = $builder->getForm();
        $page_count = $this->session->get('eccube.admin.manufacturer.search.page_count',
            $this->eccubeConfig->get('eccube_default_page_count'));

        $page_count_param = (int) $request->get('page_count');
        $pageMaxis = $this->pageMaxRepository->findAll();

        if ($page_count_param) {
            foreach ($pageMaxis as $pageMax) {
                if ($page_count_param == $pageMax->getName()) {
                    $page_count = $pageMax->getName();
                    $this->session->set('eccube.admin.manufacturer.search.page_count', $page_count);
                    break;
                }
            }
        }
        if ('POST' === $request->getMethod()) {
            $searchForm->handleRequest($request);
            if ($searchForm->isValid()) {
                /**
                 * 検索が実行された場合は, セッションに検索条件を保存する.
                 * ページ番号は最初のページ番号に初期化する.
                 */
                $page_no = 1;
                $searchData = $searchForm->getData();

                // 検索条件, ページ番号をセッションに保持.
                $this->session->set('eccube.admin.manufacturer.search', FormUtil::getViewData($searchForm));
                $this->session->set('eccube.admin.manufacturer.search.page_no', $page_no);
            } else {
                // 検索エラーの際は, 詳細検索枠を開いてエラー表示する.
                return [
                    'searchForm' => $searchForm->createView(),
                    'pagination' => [],
                    'pageMaxis' => $pageMaxis,
                    'page_no' => $page_no,
                    'page_count' => $page_count,
                    'has_errors' => true,
                ];
            }
        } else {
            if (null !== $page_no || $request->get('resume')) {
                /*
                 * ページ送りの場合または、他画面から戻ってきた場合は, セッションから検索条件を復旧する.
                 */
                if ($page_no) {
                    // ページ送りで遷移した場合.
                    $this->session->set('eccube.admin.manufacturer.search.page_no', (int) $page_no);
                } else {
                    // 他画面から遷移した場合.
                    $page_no = $this->session->get('eccube.admin.manufacturer.search.page_no', 1);
                }
                $viewData = $this->session->get('eccube.admin.manufacturer.search', []);
                $searchData = FormUtil::submitAndGetData($searchForm, $viewData);
            } else {
                /**
                 * 初期表示の場合.
                 */
                $page_no = 1;
                // submit default value
                $viewData = FormUtil::getViewData($searchForm);
                $searchData = FormUtil::submitAndGetData($searchForm, $viewData);

                // セッション中の検索条件, ページ番号を初期化.
                $this->session->set('eccube.admin.manufacturer.search', $viewData);
                $this->session->set('eccube.admin.manufacturer.search.page_no', $page_no);
            }
        }

        $qb = $this->manufacturerRepository->getQueryBuilderBySearchDataForAdmin($searchData);
        $event = new EventArgs(
            [
                'qb' => $qb,
                'searchData' => $searchData,
            ],
            $request
        );

        $this->eventDispatcher->dispatch($event, EccubeEvents::ADMIN_PRODUCT_INDEX_SEARCH);

        $sortKey = $searchData['sortkey'];

        if (empty($this->manufacturerRepository::COLUMNS[$sortKey]) || $sortKey == 'code' || $sortKey == 'status') {
            $pagination = $paginator->paginate(
                $qb,
                $page_no,
                $page_count
            );
        } else {
            $pagination = $paginator->paginate(
                $qb,
                $page_no,
                $page_count,
                ['wrap-queries' => true]
            );
        }

        return [
            'searchForm' => $searchForm->createView(),
            'pagination' => $pagination,
            'pageMaxis' => $pageMaxis,
            'page_no' => $page_no,
            'page_count' => $page_count,
            'has_errors' => false,
        ];
    }

    /**
     * @Route("/%eccube_admin_route%/manufacturer/manufacturer/new", name="admin_manufacturer_manufacturer_new", methods={"GET", "POST"})
     * @Route("/%eccube_admin_route%/manufacturer/manufacturer/{id}/edit", requirements={"id" = "\d+"}, name="admin_manufacturer_manufacturer_edit", methods={"GET", "POST"})
     * @Template("@admin/Manufacturer/manufacturer.twig")
     */
    public function edit(Request $request, RouterInterface $router, CacheUtil $cacheUtil, $id = null)
    {
        if (is_null($id)) {
            $Manufacturer = new Manufacturer();
        } else {
            $Manufacturer = $this->manufacturerRepository->find($id);
            if (!$Manufacturer) {
                throw new NotFoundHttpException();
            }
        }
        $builder = $this->formFactory
            ->createBuilder(ManufacturerType::class, $Manufacturer);
        $event = new EventArgs(
            [
                'builder' => $builder,
                'Manufacturer' => $Manufacturer,
            ],
            $request
        );
        $form = $builder->getForm();
             // ファイルの登録
        $images = [];
        $ManufacturerImages = $Manufacturer->getManufacturerImage();
        if(!is_null($ManufacturerImages)){
            foreach ($ManufacturerImages as $ManufacturerImage) {
                $images[] = $ManufacturerImage->getFileName();
            }
        }
            $form['images']->setData($images);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $Manufacturer = $form->getData();

                $this->entityManager->persist($Manufacturer);
                $this->entityManager->flush();

                 // 画像の登録
                $add_images = $form->get('add_images')->getData();
                foreach ($add_images as $add_image) {
                    $ManufacturerImage = new ManufacturerImage();
                    $ManufacturerImage
                        ->setFileName($add_image)
                        ->setManufacturer($Manufacturer)
                        ->setSortNo(1);
                    $Manufacturer->addManufacturerImage($ManufacturerImage);
                    $this->entityManager->persist($ManufacturerImage);

                    // 移動
                    $file = new File($this->eccubeConfig['eccube_temp_image_dir'].'/'.$add_image);
                    $file->move($this->eccubeConfig['eccube_save_image_dir']);
                }

                // 画像の削除
                $delete_images = $form->get('delete_images')->getData();
                $fs = new Filesystem();
                foreach ($delete_images as $delete_image) {
                    $ManufacturerImage = $this->manufacturerImageRepository->findOneBy([
                        'Manufacturer' => $Manufacturer,
                        'file_name' => $delete_image,
                    ]);

                    if ($ManufacturerImage instanceof ManufacturerImage) {
                        $Manufacturer->removeManufacturerImage($ManufacturerImage);
                        $this->entityManager->remove($ManufacturerImage);
                        $this->entityManager->flush();

                        // 他に同じ画像を参照する商品がなければ画像ファイルを削除
                        if (!$this->manufacturerImageRepository->findOneBy(['file_name' => $delete_image])) {
                            $fs->remove($this->eccubeConfig['eccube_save_image_dir'].'/'.$delete_image);
                        }
                    } else {
                        // 追加してすぐに削除した画像は、Entityに追加されない
                        $fs->remove($this->eccubeConfig['eccube_temp_image_dir'].'/'.$delete_image);
                    }
                }
                $Manufacturer->setUpdateDate(new \DateTime());
                $this->entityManager->flush();

                $this->addSuccess('admin.common.save_complete', 'admin');

                 if (array_key_exists('manufacturer_image', $request->request->get('admin_manufacturer'))) {
                    $manufacturer_image = $request->request->get('admin_manufacturer')['manufacturer_image'];
                    foreach ($manufacturer_image as $sortNo => $filename) {
                        $ManufacturerImage = $this->manufacturerImageRepository
                            ->findOneBy([
                                'file_name' => pathinfo($filename, PATHINFO_BASENAME),
                                'Manufacturer' => $Manufacturer,
                            ]);
                        if ($ManufacturerImage !== null) {
                            $ManufacturerImage->setSortNo($sortNo);
                            $this->entityManager->persist($ManufacturerImage);
                        }
                    }
                    $this->entityManager->flush();
                }

                if ($returnLink = $form->get('return_link')->getData()) {
                    try {
                        // $returnLinkはpathの形式で渡される. pathが存在するかをルータでチェックする.
                        $pattern = '/^'.preg_quote($request->getBasePath(), '/').'/';
                        $returnLink = preg_replace($pattern, '', $returnLink);
                        $result = $router->match($returnLink);
                        // パラメータのみ抽出
                        $params = array_filter($result, function ($key) {
                            return 0 !== \strpos($key, '_');
                        }, ARRAY_FILTER_USE_KEY);

                        // pathからurlを再構築してリダイレクト.
                        return $this->redirectToRoute($result['_route'], $params);
                    } catch (\Exception $e) {
                        // マッチしない場合はログ出力してスキップ.
                        log_warning('URLの形式が不正です。');
                    }
                }
                $cacheUtil->clearDoctrineCache();

                return $this->redirectToRoute('admin_manufacturer_manufacturer_edit', ['id' => $Manufacturer->getId()]);
            }
        }

        return [
            'Manufacturer' => $Manufacturer,
            'form' => $form->createView(),
            'id' => $id,
        ];
    }


    /**
     * 画像アップロード時にリクエストされるメソッド.
     *
     * @see https://pqina.nl/filepond/docs/api/server/#process
     * @Route("/%eccube_admin_route%/manufacturer/manufacturer/image/process", name="admin_manufacturer_image_process", methods={"POST"})
     */
    public function imageProcess(Request $request)
    {
        if (!$request->isXmlHttpRequest() && $this->isTokenValid()) {
            throw new BadRequestHttpException();
        }

        $images = $request->files->get('admin_manufacturer');

        $allowExtensions = ['gif', 'jpg', 'jpeg', 'png'];
        $files = [];
        if (count($images) > 0) {
            foreach ($images as $img) {
                foreach ($img as $image) {
                    // ファイルフォーマット検証
                    $mimeType = $image->getMimeType();
                    if (0 !== strpos($mimeType, 'image')) {
                        throw new UnsupportedMediaTypeHttpException();
                    }

                    // 拡張子
                    $extension = $image->getClientOriginalExtension();
                    if (!in_array(strtolower($extension), $allowExtensions)) {
                        throw new UnsupportedMediaTypeHttpException();
                    }

                    $filename = date('mdHis').uniqid('_').'.'.$extension;
                    $image->move($this->eccubeConfig['eccube_temp_image_dir'], $filename);
                    $files[] = $filename;
                }
            }
        }

        $event = new EventArgs(
            [
                'images' => $images,
                'files' => $files,
            ],
            $request
        );
        $this->eventDispatcher->dispatch($event, EccubeEvents::ADMIN_PRODUCT_ADD_IMAGE_COMPLETE);
        $files = $event->getArgument('files');

        return new Response(array_shift($files));
    }


    /**
     * アップロード画像を取得する際にコールされるメソッド.
     *
     * @see https://pqina.nl/filepond/docs/api/server/#load
     * @Route("/%eccube_admin_route%/manufacturer/manufacturer/image/load", name="admin_manufacturer_image_load", methods={"GET"})
     */
    public function imageLoad(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException();
        }

        $dirs = [
            $this->eccubeConfig['eccube_save_image_dir'],
            $this->eccubeConfig['eccube_temp_image_dir'],
        ];

        foreach ($dirs as $dir) {
            if (strpos($request->query->get('source'), '..') !== false) {
                throw new NotFoundHttpException();
            }
            $image = \realpath($dir.'/'.$request->query->get('source'));
            $dir = \realpath($dir);

            if (\is_file($image) && \str_starts_with($image, $dir)) {
                $file = new \SplFileObject($image);

                return $this->file($file, $file->getBasename());
            }
        }

        throw new NotFoundHttpException();
    }


    /**
     * アップロード画像をすぐ削除する際にコールされるメソッド.
     *
     * @see https://pqina.nl/filepond/docs/api/server/#revert
     * @Route("/%eccube_admin_route%/manufacturer/manufacturer/image/revert", name="admin_manufacturer_image_revert", methods={"DELETE"})
     */
    public function imageRevert(Request $request)
    {
        if (!$request->isXmlHttpRequest() && $this->isTokenValid()) {
            throw new BadRequestHttpException();
        }

        $tempFile = $this->eccubeConfig['eccube_temp_image_dir'].'/'.$request->getContent();
        if (is_file($tempFile) && stripos(realpath($tempFile), $this->eccubeConfig['eccube_temp_image_dir']) === 0) {
            $fs = new Filesystem();
            $fs->remove($tempFile);

            return new Response(null, Response::HTTP_NO_CONTENT);
        }

        throw new NotFoundHttpException();
    }

    /**
     * Bulk public action
     *
     * @Route("/%eccube_admin_route%/manufacturer/bulk/manufacturer-status/{id}", requirements={"id" = "\d+"}, name="admin_manufacturer_bulk_manufacturer_status", methods={"POST"})
     *
     * @param Request $request
     * @param ManufacturerStatus $ManufacturerStatus
     *
     * @return RedirectResponse
     */
    public function bulkManufacturerStatus(Request $request, CacheUtil $cacheUtil)
    {
        $this->isTokenValid();

        /** @var Product[] $Products */
        $Products = $this->manufacturerRepository->findBy(['id' => $request->get('ids')]);
        $count = 0;
        foreach ($Products as $Product) {
            try {
                $this->manufacturerRepository->save($Product);
                $count++;
            } catch (\Exception $e) {
                $this->addError($e->getMessage(), 'admin');
            }
        }
        try {
            if ($count) {
                $this->entityManager->flush();
                $msg = $this->translator->trans('admin.product.bulk_change_status_complete', [
                    '%count%' => $count,
                ]);
                $this->addSuccess($msg, 'admin');
                $cacheUtil->clearDoctrineCache();
            }
        } catch (\Exception $e) {
            $this->addError($e->getMessage(), 'admin');
        }

        return $this->redirectToRoute('admin_manufacturer', ['resume' => Constant::ENABLED]);
    }
}
