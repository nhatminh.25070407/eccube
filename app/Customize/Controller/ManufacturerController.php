<?php

namespace Customize\Controller;

use Customize\Repository\ManufacturerRepository;
use Eccube\Controller\AbstractController;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ManufacturerController extends AbstractController
{
    protected ManufacturerRepository $manufacturerRepository;

    public function __construct(
        ManufacturerRepository $manufacturerRepository
    ) {
        $this->manufacturerRepository = $manufacturerRepository;
    }

    /**
     * 商品一覧画面.
     *
     * @Route("/manufacturer/list", name="manufacturer_list", methods={"GET"})
     * @Template("manufacturer/manufacturer_list.twig")
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $Manufacturer = $this->manufacturerRepository->findAll();

        return [
            'Manufacturer' => $Manufacturer
        ];
    }

    /**
     * 商品一覧画面.
     *
     * @Route("/manufacturer/detail/{id}", name="manufacturer_detail", methods={"GET"}, requirements={"id" = "\d+"})
     * @Template("Manufacturer/manufacturer_detail.twig")
     */
    public function detail(Request $request, $id = null)
    {
        return [];
    }
}
