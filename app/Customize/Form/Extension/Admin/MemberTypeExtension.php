<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Customize\Form\Extension\Admin;

use Customize\Entity\Manufacturer;
use Customize\Repository\ManufacturerRepository;
use Eccube\Common\EccubeConfig;
use Eccube\Form\Type\Admin\MemberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;

class MemberTypeExtension extends AbstractTypeExtension
{
    /**
     * @var EccubeConfig
     */
    private $eccubeConfig;

    /**
     * @var ManufacturerRepository
     */
    protected $manufacturerRepository;

    /**
     * MemberTypeExtension constructor.
     *
     * @param EccubeConfig $eccubeConfig
     * @param ManufacturerRepository $manufacturerRepository
     */
    public function __construct(EccubeConfig $eccubeConfig, ManufacturerRepository $manufacturerRepository)
    {
        $this->eccubeConfig = $eccubeConfig;
        $this->manufacturerRepository = $manufacturerRepository;

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('Manufacturer', EntityType::class, [
                'label' => 'admin.manufacturer',
                'class' => Manufacturer::class,
                'choice_label' => 'name',
                'mapped' => true,
                'choices' => $this->manufacturerRepository->findAll(),
                'placeholder' => 'オプションを選択',
                'eccube_form_options' => [
                    'auto_render' => false,
                ],
                'attr'=>[
                    'disabled'=> true
                ],
            ])
             ->addEventListener(FormEvents::POST_SUBMIT, function ($event) {
                $form = $event->getForm();
                $data = $form->getData();
                if ($data['Authority']) {
                    if ($data['Authority']->getId() === 2 && is_null($data['Manufacturer']))
                        $form['Manufacturer']->addError(new FormError(trans('製造者に入力されていません。')));
                }
            });

    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return MemberType::class;
    }

    /**
     * Return the class of the type being extended.
     */
    public static function getExtendedTypes(): iterable
    {
        return [MemberType::class];
    }
}
