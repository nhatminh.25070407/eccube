<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Customize\Form\Extension\Admin;

use Customize\Entity\Manufacturer;
use Customize\Repository\ManufacturerRepository;
use Eccube\Common\EccubeConfig;
use Eccube\Form\Type\Admin\ProductType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductTypeExtension extends AbstractTypeExtension
{
    /**
     * @var EccubeConfig
     */
    private $eccubeConfig;

    /**
     * @var ManufacturerRepository
     */
    protected $manufacturerRepository;

    /**
     * @var Queries
     */
    protected $security;

    /**
     * ProductTypeExtension constructor.
     *
     * @param EccubeConfig $eccubeConfig
     * @param ManufacturerRepository $manufacturerRepository
     */
    public function __construct(EccubeConfig $eccubeConfig, ManufacturerRepository $manufacturerRepository, Security $security
)
    {
        $this->eccubeConfig = $eccubeConfig;
        $this->manufacturerRepository = $manufacturerRepository;
        $this->security = $security;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();

        if ($user instanceof UserInterface) {
            if ($user->getAuthority()->getId() == 2) {
        $builder
            ->add('Manufacturer', EntityType::class, [
                'label' => 'admin.manufacturer',
                'class' => Manufacturer::class,
                'choice_label' => 'name',
                'mapped' => true,
                'choices' => $this->manufacturerRepository->findAll(),
                'placeholder' => 'オプションを選択',
                'eccube_form_options' => [
                    'auto_render' => false,
                ],
                'attr'=>[
                    'disabled' => true,
                ],
                'data' => $user->getManufacturer(),
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function ($event) use ($user) {
                $form = $event->getForm();
                $data = $form->getData();
                $data->setManufacturer($user->getManufacturer());
                $event->setData($data);
            });
        }
        else {
             $builder
            ->add('Manufacturer', EntityType::class, [
                'label' => 'admin.manufacturer',
                'class' => Manufacturer::class,
                'choice_label' => 'name',
                'mapped' => true,
                'choices' => $this->manufacturerRepository->findAll(),
                'placeholder' => 'オプションを選択',
                'eccube_form_options' => [
                    'auto_render' => false,
                ],
                'attr'=>[
                    'required' => 'required',
                ],
                'constraints' => [
                     new Assert\NotBlank(),
                ],
            ]);
        }
    }
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return ProductType::class;
    }

    /**
     * Return the class of the type being extended.
     */
    public static function getExtendedTypes(): iterable
    {
        return [ProductType::class];
    }
}
